package cn.stylefeng.guns.config.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.stylefeng.guns.core.common.constant.JwtConstants;
import cn.stylefeng.guns.core.util.JwtTokenUtil;
import io.jsonwebtoken.Claims;

/**
 * 解析生成token的参数
 * @author zhaozhenshe
 *
 */
public class GetTokenInfo {
	/**
	 * 从token中解析参数
	 * @param request
	 * @return
	 */
	public static String getUserId(HttpServletRequest request) {
		 final String requestHeader = request.getHeader(JwtConstants.AUTH_HEADER);
	     String authToken = requestHeader.substring(7);
	     Claims claims = JwtTokenUtil.getClaimFromToken(authToken);
	     String userId = claims.getSubject();
	     return userId;
	}
}
