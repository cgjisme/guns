package cn.stylefeng.guns.modular.system.entity;

import java.beans.Transient;
import java.util.Map;

import lombok.Data;

@Data
public class VechicleInfo {
	
	private Long id;
	
	private String plateNumber;
	
	private String vehicleType;
	
	private String purpose;
	
	private String status;
	
	private String createBy;
	
	private String createTime;
	
	private String updateBy;
	
	private String updateTime;
	
	private String admin;
	
	private String regionForUse;
	
}
