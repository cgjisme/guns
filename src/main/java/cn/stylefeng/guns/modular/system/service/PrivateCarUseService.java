package cn.stylefeng.guns.modular.system.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import cn.stylefeng.guns.modular.system.entity.PrivateCarUse;
import cn.stylefeng.guns.modular.system.mapper.PrivateCarUseMapper;
import cn.stylefeng.roses.core.reqres.response.ResponseData;

@Transactional
@Service
public class PrivateCarUseService {
	
	@Autowired
	private PrivateCarUseMapper privateCarUseMapper;
	
	public int savePrivateCarUseInfo(PrivateCarUse privateCarUse) {
		return privateCarUseMapper.savePrivateCarUseInfo(privateCarUse);
	}
	
	
	/**
	 * 私车公用 待校对申请信息查询
	 * @param userId
	 * @return
	 */
	public Page<Map<String, Object>> toBeProofread(Integer region,Page page,String uri ) {
		return privateCarUseMapper.toBeProofread(page,region,uri);
	}
	/**
	 * 查询用户所属区域
	 * @param userId
	 * @return
	 */
	public Integer getUserRegion(String userId) {
		return privateCarUseMapper.getUserRegion(userId);
	}
	/**
	 * 私车公用 部门总监待审核申请查询
	 * @param userId
	 * @return
	 */
	public Page<Map<String, Object>> findPriCarUserInfoByDeptAdmin(String userId,Page page,String uri ){
		return privateCarUseMapper.findPriCarUserInfoByDeptAdmin(page, userId,uri);
	}
	
	public List<Integer> whetherNotify(String userId) {
		return privateCarUseMapper.whetherNotify(userId);
	}
	
	/**
	 * 私车公用 知会领导待审核申请查询
	 * @param userId
	 * @param page
	 * @return
	 */
	public Page<Map<String, Object>> findNotifyAdminAuditInfo(List<Integer> region,Page page,String uri){
		return privateCarUseMapper.findNotifyAdminAuditInfo(page, region, uri);
	}
	
	public int updateStatus( PrivateCarUse privateCarUse) {
		return privateCarUseMapper.updateStatus(privateCarUse);
	}
	
	/**
	 * 私车公用个人申请查询
	 * @param userId
	 * @return
	 */
	public Page<Map<String, Object>>  getAllApplyInfoByUserId(Page page,String userId) {
		return privateCarUseMapper.getAllApplyInfoByUserId(page, userId);
	}
	
	public Page<Map<String, Object>> getAllApproval(Page page,String userId,String uri) {
		return privateCarUseMapper.getAllApproval(page, userId,uri);
	}
	
	/**
	 * 公务用车个人申请查询
	 * @param userId
	 * @return
	public Page<Map<String, Object>>  getAllApplyInfoByUserId2(String userId) {
		return privateCarUseMapper.getAllApplyInfoByUserId(userId);
	}
	*/
	/**
	 * 申请延期
	 * @param privateCarUse
	 * @return
	 */
	public int delayApply(PrivateCarUse privateCarUse) {
		return privateCarUseMapper.delayApply(privateCarUse);
	}
	/**
	 * 私车公用 还车
	 * @return
	 */
	public int returnVechiclePrivate(PrivateCarUse privateCarUse) {
		return privateCarUseMapper.returnVechiclePrivate(privateCarUse);
	}
	
	
	public Map<String, Object> getPrivInfoById(String id,String uri){
		return privateCarUseMapper.getPrivInfoById(id, uri);
	}
	
	public int cancelApplyPrivate(Long id) {
		return privateCarUseMapper.cancelApplyPrivate(id);
	}
}
