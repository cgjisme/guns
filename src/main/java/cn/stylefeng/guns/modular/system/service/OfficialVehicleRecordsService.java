package cn.stylefeng.guns.modular.system.service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import cn.stylefeng.guns.core.common.page.LayuiPageFactory;
import cn.stylefeng.guns.modular.system.entity.OfficialVehicleRecords;
import cn.stylefeng.guns.modular.system.entity.PrivateCarUse;
import cn.stylefeng.guns.modular.system.mapper.OfficialVehicleRecordsMapper;
import cn.stylefeng.guns.modular.system.mapper.VechicleInfoMappper;
import cn.stylefeng.guns.modular.system.model.UserDto;
import cn.stylefeng.roses.core.reqres.response.ResponseData;

@Service
@Transactional
public class OfficialVehicleRecordsService {
	
	@Autowired
	private OfficialVehicleRecordsMapper officialVehicleRecordsMapper;
	
	@Autowired
	private VechicleInfoMappper vechicleInfoMappper;
	
	@Autowired
	private YmlUtils ymlUtils;
	
	public void saveApplyInfo(OfficialVehicleRecords officialVehicleRecords) {
//		officialVehicleRecords.setCreateTime(null);
		//申请表新增记录
		officialVehicleRecordsMapper.saveApplyInfo(officialVehicleRecords);
		//修改车辆表中该车辆的状态
		vechicleInfoMappper.updateVechicleStatus(officialVehicleRecords.getVechicleId(), "3");
	}
	
	
	public UserDto queryUserRole(String userId){
		return officialVehicleRecordsMapper.queryUserRole(userId);
	}
	/**
	 * 查询用户是否是部门管理员
	 * @param userId
	 * @return
	 */
	public int getUserDeptAdmin(String userId) {
		return officialVehicleRecordsMapper.getUserDeptAdmin(userId);
	}
	/**
	 * 查询用户部门下所有未审核的公务用车申请
	 * @param userId
	 * @return
	 */
	public Page<Map<String, Object>> getDeptOfficeApply(String userId,Page page,String uri){
//		Page page = LayuiPageFactory.defaultPage();
		return officialVehicleRecordsMapper.getDeptOfficeApply(page,userId,uri);
	}
	/**
	 * 查询用户是否是车辆管理员
	 * @param userId
	 * @return
	 */
	public int getIsVechicleAdmin(String userId) {
		return officialVehicleRecordsMapper.getIsVechicleAdmin(userId);
	}
	/**
	 * 车辆管理员查看审核信息
	 * @return
	 */
	public Page<Map<String, Object>> getAplyVechicleAdmin(String userId,Page page,String uri){
		return officialVehicleRecordsMapper.getAplyVechicleAdmin(page,userId,uri);
	}
	/**
	 * 用户关联信息查询
	 * @param userId
	 * @return
	 */
	public List<String> userRelationships(String userId){
		return officialVehicleRecordsMapper.userRelationships(userId);
	}
	/**
	 * 后勤主管人员审批
	 * @param userId
	 * @return
	 */
	public Page<Map<String, Object>>  personInCharge(Page page,String userId,String uri){
		return officialVehicleRecordsMapper.personInCharge(page,userId, uri);
	}
	/**
	 * 查询综管总监审批记录和可审核的申请
	 * @param userId
	 * @return
	 */
	public Page<Map<String, Object>>  comprehensiveAdmin(Page page,String uri){
		return officialVehicleRecordsMapper.comprehensiveAdmin(page,uri);
	}
	
	public int isComprehensiveAdmin(String userId) {
		return officialVehicleRecordsMapper.isComprehensiveAdmin(userId);
	}
	/**
	 * 管理员审核
	 * @return
	 */
	public int adminAudit(OfficialVehicleRecords officialVehicleRecords) {
		return officialVehicleRecordsMapper.deptAdminExamine(officialVehicleRecords);
	}
	
	public String getSuperiorUser(String userId) {
		return officialVehicleRecordsMapper.getSuperiorUser(userId);
	}
	/**
	 * 申请延期
	 * @param privateCarUse
	 * @return
	 */
	public int delayApply(PrivateCarUse privateCarUse) {
		return officialVehicleRecordsMapper.delayApply(privateCarUse);
	}
	/**
	 * 公务用车 立即还车
	 * @param privateCarUse
	 * @return
	 * @throws IOException 
	 * @throws IllegalStateException 
	 */
	public Object returnVechicleOffice(OfficialVehicleRecords officialVehicleRecords,
			MultipartFile file ,String type) {
		String fileName = System.currentTimeMillis() + file.getOriginalFilename();
//		String fileName2 = System.currentTimeMillis() + appearanceFile.getOriginalFilename();
        //文件上传路径拼接
        String destFileName = ymlUtils.getMileage();
//        String destFileName2 = ymlUtils.getAppearance();
        //第一次运行的时候，这个文件所在的目录往往是不存在的，这里需要创建一下目录
        File  mileageFilePath= new File(destFileName);
//        File  appearanceFilePath= new File(destFileName2);
        //如果文件夹不存在则创建    
		if (!mileageFilePath.exists() && !mileageFilePath.isDirectory()) {
			System.out.println("//不存在");
			mileageFilePath.mkdir();
		} else {
			System.out.println("//目录存在");
		}
//		if (!appearanceFilePath.exists() && !appearanceFilePath.isDirectory()) {
//			System.out.println("//不存在");
//			appearanceFilePath.mkdir();
//		} else {
//			System.out.println("//目录存在");
//		}
		try {
			FileUtil.uploadFile(file.getBytes(), destFileName, fileName);
		} catch (Exception e) {
			e.printStackTrace();
			return "图片上传失败"+e.getMessage();
		}
//		try {
//			FileUtil.uploadFile(appearanceFile.getBytes(), destFileName2, fileName2);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return "车辆外观图片上传失败"+e.getMessage();
//		}
		if ("1".equals(type)) {
			officialVehicleRecords.setMileageUrl(destFileName+fileName);
		}else if("2".equals(type)){
			officialVehicleRecords.setAppearanceUrl(destFileName+fileName);
		}
		officialVehicleRecordsMapper.returnVechicleOffice(officialVehicleRecords);
		//修改车辆状态为可用
		int i = officialVehicleRecordsMapper.updateVechilcleStatus(officialVehicleRecords.getId(),"1");
		return i;
	}
	
//	public void updateVechicleStatus(Long id , String status) {
//		vechicleInfoMappper.updateVechicleStatus(id, status);
//	}
	/**
	 * 根据公务用车申请表id联合修改车辆状态
	 * @param id
	 * @param status
	 * @return
	 */
	public int updateVechilcleStatusByOffApplyId(Long id , String status) {
		return officialVehicleRecordsMapper.updateVechilcleStatus(id, status);
	}
	/**
	 * 我的审批  公务用车
	 * @param request
	 * @return
	 */
	public Page<Map<String, Object>> getAllApproval(Page page,String userId) {
		
		return officialVehicleRecordsMapper.getAllApproval(page, userId,ymlUtils.getUri());
	}
	
	/**
	 * 公务用车个人申请查询
	 * @param userId
	 * @return
	 */
	public Page<Map<String, Object>>  getAllApplyInfoByUserId(Page page,String userId) {
		return officialVehicleRecordsMapper.getAllApplyInfoByUserId(page,userId);
	}
	
	public int queryDriverAndIDCard(String userId) {
		return officialVehicleRecordsMapper.queryDriverAndIDCard(userId);
	}
	
	public Object uploadImages(MultipartFile file, String type,String userId) throws IOException, Exception {
		UserDto userDto = new UserDto();
		userDto.setUserId(Long.valueOf(userId));
		String fileName = System.currentTimeMillis() + file.getOriginalFilename();
		//1: 驾驶证    2：身份证正面 3：身份证反面 4：头像
		if("1".equals(type)) {
			String path = ymlUtils.getDriverLicense();
			File  destFile= new File(path);
	        //如果文件夹不存在则创建    
			if (!destFile.exists() && !destFile.isDirectory()) {
				System.out.println("//不存在");
				destFile.mkdir();
			} else {
				System.out.println("//目录存在");
			}
			FileUtil.uploadFile(file.getBytes(), path, fileName);
			userDto.setDriverLicenseUrl(path+fileName);
		}else if("2".equals(type)) {
			String path = ymlUtils.getIdentityCard();
			File  destFile= new File(path);
	        //如果文件夹不存在则创建    
			if (!destFile.exists() && !destFile.isDirectory()) {
				System.out.println("//不存在");
				destFile.mkdir();
			} else {
				System.out.println("//目录存在");
			}
			FileUtil.uploadFile(file.getBytes(), path, fileName);
			userDto.setIdCardFrontUrl(path+fileName);
		}
		else if("3".equals(type)) {
			String path = ymlUtils.getIdentityCard();
			File  destFile= new File(path);
	        //如果文件夹不存在则创建    
			if (!destFile.exists() && !destFile.isDirectory()) {
				System.out.println("//不存在");
				destFile.mkdir();
			} else {
				System.out.println("//目录存在");
			}
			FileUtil.uploadFile(file.getBytes(), path, fileName);
			userDto.setIdCardBackUrl(path+fileName);
		}
		else if("4".equals(type)) {
			String path = ymlUtils.getIdentityCard();
			File  destFile= new File(path);
	        //如果文件夹不存在则创建    
			if (!destFile.exists() && !destFile.isDirectory()) {
				System.out.println("//不存在");
				destFile.mkdir();
			} else {
				System.out.println("//目录存在");
			}
			FileUtil.uploadFile(file.getBytes(), path, fileName);
			userDto.setAvatar(path+fileName);
		}
		return officialVehicleRecordsMapper.updateUserImgInfoByUserId(userDto);
	}
	
	public Map<String, Object> getOffInfoById(@Param("id") String id,@Param("uri") String uri){
		return officialVehicleRecordsMapper.getOffInfoById(id, uri);
	}
	
	public int cancelApplyOffical(Long id) {
		officialVehicleRecordsMapper.cancelApplyOffical(id);
		//修改车辆状态为可用
		int i = officialVehicleRecordsMapper.updateVechilcleStatus(id,"1");
		return i;
	}
}
