package cn.stylefeng.guns.modular.system.entity;

import lombok.Data;

@Data
public class PrivateCarUse {
	
	private Long id;
	
	private String plateNumber;
	
	private String displacement;
	
	private String startTime;
	
	private String returnTime;
	
	private String placeOfDeparture;
	
	private String destination;
	
	private String initialMileage;
	
	private String endMileage;
	
	private String peopleTogether;
	
	private String applicationReasons;
	
	private Integer status;
	
	private Long applyUser;
	
	private String createdTime;
	
	private String region;
	
	private String applyName;
	
	private String step;
	
	private String  auditStatus;
	
	private String  type;
	
	
	
	
	private Integer proofStatus;
	
	private String  proofUser;
	
	private Integer deptAuditStatus;
	
	private String  deptAuditUser;
	
	private Integer logisticsStatus;
	
	private String  logisticsUser;
	
	private Integer notifyStatus;
	
	private String  notifyUser;
	
}
