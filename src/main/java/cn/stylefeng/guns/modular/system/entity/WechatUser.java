package cn.stylefeng.guns.modular.system.entity;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import lombok.Data;

@Data
public class WechatUser {
	 /**
	 * 主键id
	 */
    private Long userId;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 账号
     */
    private String account;
    /**
     * 密码
     */
    private String password;
    /**
     * md5密码盐
     */
    private String salt;
    /**
     * 名字
     */
    private String name;
    /**
     * 生日
     */
    private Date birthday;
    /**
     * 性别(字典)
     */
    private String sex;
    /**
     * 电子邮件
     */
    private String email;
    /**
     * 电话
     */
    private String phone;
    /**
     * 角色id(多个逗号隔开)
     */
    private String roleId;
    /**
     * 部门id(多个逗号隔开)
     */
    private Long deptId;
    /**
     * 状态(字典)
     */
    private String status;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 创建人
     */
    private Long createUser;
    /**
     * 更新时间
     */
    private Date updatedTime;
    /**
     * 更新人
     */
    private Long updateUser;
    /**
     * 乐观锁
     */
    private Integer version;
    
    private String idCardFrontUrl;
    private String idCardBackUrl;
    private String driverLicenseUrl;
    
    private String deptName;
    
}
