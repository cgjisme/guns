package cn.stylefeng.guns.modular.system.model;

import java.beans.Transient;
import java.util.Map;

import lombok.Data;

@Data
public class VechicleInfoDto {
	
	private Long id;
	
	private String plateNumber;
	
	private String vehicleType;
	
	private String purpose;
	
	private String status;
	
	private String createBy;
	
	private String createTime;
	
	private String updateBy;
	
	private String updateTime;
	
	private String admin;
	
	private String regionForUse;
	
	private String isSendCar;
	
	private Map<String,String> adminMap;
	
}
