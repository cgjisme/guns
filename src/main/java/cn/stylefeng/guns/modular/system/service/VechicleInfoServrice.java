package cn.stylefeng.guns.modular.system.service;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import cn.stylefeng.guns.core.common.page.LayuiPageFactory;
import cn.stylefeng.guns.core.shiro.ShiroKit;
import cn.stylefeng.guns.core.shiro.ShiroUser;
import cn.stylefeng.guns.modular.system.entity.Role;
import cn.stylefeng.guns.modular.system.entity.User;
import cn.stylefeng.guns.modular.system.entity.VechicleInfo;
import cn.stylefeng.guns.modular.system.mapper.RoleMapper;
import cn.stylefeng.guns.modular.system.mapper.UserMapper;
import cn.stylefeng.guns.modular.system.mapper.VechicleInfoMappper;
import cn.stylefeng.guns.modular.system.model.VechicleInfoDto;
import cn.stylefeng.roses.core.reqres.response.ResponseData;
import cn.stylefeng.roses.core.util.ToolUtil;
import cn.stylefeng.roses.kernel.model.exception.RequestEmptyException;
/**
 * 
 * @author zhaozhenshe
 *
 */
@Component
public class VechicleInfoServrice extends ServiceImpl<VechicleInfoMappper, VechicleInfo> {
	
	@Autowired
	private VechicleInfoMappper infoMappper;
	
	@Autowired
	private UserMapper userMapper;
	 /**
     * 根据条件查询车辆信息
     * @return
     */
    public Page<Map<String, Object>> selectVechPage( String roleName) {
        Page page = LayuiPageFactory.defaultPage();
        return infoMappper.selectVechicleInfos(page,roleName);
    }
    
    public void remove (Long id) {
    	infoMappper.updateVechicleStatus(id, "0");
    }
    /**
     * 查询所有的车辆管理员
     * @return
     */
    public List<User> queryVechicleAdmin(){
    	return infoMappper.queryVechicleAdmin();
    }
    
    @Transactional(rollbackFor = Exception.class)
    public void vechicleUpdate(VechicleInfoDto vechicleInfoDto){
//    	this.assertAuth(user.getUserId());
    	ShiroUser shiroUser = ShiroKit.getUserNotNull();
    	Long id = shiroUser.getId();
    	vechicleInfoDto.setUpdateBy(id.toString());
    	infoMappper.vechicleUpdate(vechicleInfoDto);
    }
    public void addVechicle(VechicleInfoDto vechicleInfo) {
   	 	ShiroUser shiroUser = ShiroKit.getUserNotNull();
   	 	Long id = shiroUser.getId();
   	 	vechicleInfo.setCreateBy(id.toString());
   	 	infoMappper.addVechicle(vechicleInfo);
    }
    /**
     * 车辆类型查询
     * @param vechicleInfo
     * @return
     */
    public List<String> queryVechicleType() {
    	return infoMappper.queryVechicleType();
    }
    
    public List<VechicleInfoDto> queryPlateNumber(String type){
    	return infoMappper.queryPlateNumber(type);
    }
    
}
