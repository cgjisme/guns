package cn.stylefeng.guns.modular.system.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import cn.stylefeng.guns.core.interceptor.RestApiInteceptor;
import cn.stylefeng.guns.modular.system.entity.OfficialVehicleRecords;
import cn.stylefeng.guns.modular.system.entity.PrivateCarUse;
import cn.stylefeng.guns.modular.system.service.OfficialVehicleRecordsService;
import cn.stylefeng.guns.modular.system.service.PrivateCarUseService;
import cn.stylefeng.guns.modular.system.service.YmlUtils;
import cn.stylefeng.roses.core.base.controller.BaseController;
import cn.stylefeng.roses.core.reqres.response.ResponseData;


@RestController
@RequestMapping("/privateCarUse")
public class PrivateCarUseController extends BaseController{
	@Autowired
	private YmlUtils ymlUtils;
	
	@Autowired
	private PrivateCarUseService privateCarUseService;
	@Autowired
	private OfficialVehicleRecordsService officialVehicleRecordsService;
	
	/**
	 * 私车公用申请
	 * @param officialVehicleRecords
	 * @return
	 */
	@PostMapping("/apply")
	public ResponseData vechicleAdd(@RequestBody PrivateCarUse privateCarUse,HttpServletRequest request) {
		String userId = RestApiInteceptor.getUserId(request);
		privateCarUse.setApplyUser(Long.valueOf(userId));
	    return ResponseData.success(privateCarUseService.savePrivateCarUseInfo(privateCarUse));
	}
	
	/**
	 * 私车公用可审核车辆信息查询
	 * @param request
	 * @return
	 */
	@GetMapping("/director")
	public ResponseData privateUseCarAudit(Page page,HttpServletRequest request) {
		String userId = RestApiInteceptor.getUserId(request);
		String uri = ymlUtils.getUri();
		Integer region = privateCarUseService.getUserRegion(userId);
		if (region != null) {
			Page<Map<String, Object>> list = privateCarUseService.toBeProofread(region,page,uri);
			return ResponseData.success(list);
		}
		//	//查询用户是否是部门管理员
		int i = officialVehicleRecordsService.getUserDeptAdmin(userId);
		if(i==1) {
			return ResponseData.success(privateCarUseService.findPriCarUserInfoByDeptAdmin(userId, page,uri));
		}
//		List<Integer> region2 = privateCarUseService.whetherNotify(userId);
//		if (region2 != null && region2.size() != 0) {
//			return ResponseData.success(privateCarUseService.findNotifyAdminAuditInfo(region2, page, uri));
//			
//		}
		return ResponseData.success(null);
	}
	
	
	/**
	 * 私车公用  审核
	 * @param request
	 * @return
	 */
	@PostMapping("/update/status")
	public ResponseData updateStatus(@RequestBody PrivateCarUse privateCarUse,HttpServletRequest request) {
		String userId = RestApiInteceptor.getUserId(request);
		//查询用户是否属于区域管理者
		Integer region = privateCarUseService.getUserRegion(userId);
		if (region != null && !"".equals(privateCarUse.getStep()) && privateCarUse.getStep() != null) {
			if ("1".equals(privateCarUse.getStep())) {
				privateCarUse.setProofStatus(privateCarUse.getStatus());
				privateCarUse.setProofUser(userId);
				privateCarUseService.updateStatus(privateCarUse);
				return SUCCESS_TIP;
			}else if("2".equals(privateCarUse.getStep())){
				privateCarUse.setLogisticsStatus(privateCarUse.getStatus());
				privateCarUse.setLogisticsUser(userId);
				privateCarUseService.updateStatus(privateCarUse);
				return SUCCESS_TIP;
			}
		}
		//查询用户是否是部门总监
		int j = officialVehicleRecordsService.getUserDeptAdmin(userId);
		if (j==1) {
			privateCarUse.setDeptAuditStatus(privateCarUse.getStatus());
			privateCarUse.setDeptAuditUser(userId);
			privateCarUseService.updateStatus(privateCarUse);
			return SUCCESS_TIP;
		}
//		List<Integer> region2 = privateCarUseService.whetherNotify(userId);
//		if (region2 != null && region2.size() != 0) {
//			privateCarUse.setNotifyStatus(privateCarUse.getStatus());
//			privateCarUse.setNotifyUser(userId);
//			privateCarUseService.updateStatus(privateCarUse);
//			return SUCCESS_TIP;
//		}
		return null;
	}
	/**
	 * 查询用户所有的申请记录 私车公用
	 * @param request
	 * @return
	 */
	@GetMapping("/allApplyInfo")
	public ResponseData getAllApplyInfoByUserId(HttpServletRequest request,Page page) {
		String userId = RestApiInteceptor.getUserId(request);
		return ResponseData.success(privateCarUseService.getAllApplyInfoByUserId(page, userId));
		
	}
	
	/**
	 * 查看用户已审批通过的所有记录 私车公用
	 * @param request
	 * @param page
	 * @return
	 */
	@GetMapping("/applyInfo")
	public ResponseData getAllApproval(HttpServletRequest request,Page page) {
		String userId = RestApiInteceptor.getUserId(request);
		String uri = ymlUtils.getUri();
		return ResponseData.success(privateCarUseService.getAllApproval(page, userId,uri));
	}
	/**
	 * 申请延期
	 * @param privateCarUse
	 * @return
	 */
	@PostMapping("/delay")
	public ResponseData delayApply(@RequestBody PrivateCarUse privateCarUse) {
		
		//type:1 公务用车   2：私车公用
		if ("2".equals(privateCarUse.getType())) {
			privateCarUseService.delayApply(privateCarUse);
			return SUCCESS_TIP;
		}else if("1".equals(privateCarUse.getType())){
			officialVehicleRecordsService.delayApply(privateCarUse);
			return SUCCESS_TIP;
		}
		return null;
	}
	
	
	@GetMapping("/getInfoById")
	public ResponseData getInfoById(String id) {
		String uri = ymlUtils.getUri();
		return ResponseData.success(privateCarUseService.getPrivInfoById(id,uri));
	}
	
	
}
