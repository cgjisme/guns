package cn.stylefeng.guns.modular.system.entity;

import lombok.Data;

@Data
public class OfficialVehicleRecords {
	
	private long id;
	
	private Long vechicleId;
	
	private String startTime;
	
	private String returnTime;
	
	private String placeOfDeparture;
	
	private String destination;
	
	private String expectedMileage;
	
	private String actualMileage;
	
	private String  peopleTogether;
	
	private String  applicationReasons;
	
	private String  status;
	
	private String  applyUser;
	
	private String  createdTime;
	
	private String  type;
	
	private String  useTime;
	
	private String  vechicleAdminStatus;
	
	private String  vechicleAdminUser;
	
	private String  auditStatus;
	
	private String  logisticsStatus;
	
	private String  logisticsUser;
	
	private String  deptReviewedUser;
	
	private String  synthesizeUser;
	
	private String  synthesizeStatus;
	
	private String reason;
	
	private String mileageUrl;
	
	private String appearanceUrl;
	
	private Integer isIntact;
	
	private Integer oilQuantity;
}
