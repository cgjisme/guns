package cn.stylefeng.guns.modular.system.service;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@ConfigurationProperties(prefix = "imagepath")
@Data
public class YmlUtils {
		private String mileage;
		private String appearance;
		private String driverLicense;
		private String identityCard;
		private String uri;
}
