package cn.stylefeng.guns.modular.system.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import cn.stylefeng.guns.modular.system.entity.Dept;
import cn.stylefeng.guns.modular.system.entity.OfficialVehicleRecords;
import cn.stylefeng.guns.modular.system.entity.PrivateCarUse;
import cn.stylefeng.guns.modular.system.model.UserDto;

public interface OfficialVehicleRecordsMapper extends BaseMapper<OfficialVehicleRecords>{
	
	public int saveApplyInfo(OfficialVehicleRecords officialVehicleRecords);
	
	/**
	 * 查询用户的角色
	 * @param userId
	 * @return
	 */
	public UserDto queryUserRole(String userId);
	/**
	 * 查询用户是否是部门管理员
	 * @param userId
	 * @return
	 */
	public int getUserDeptAdmin(String userId);
	/**
	 * 查询用户部门下所有未审核的公务用车申请
	 * @param userId
	 * @return
	 */
	public Page<Map<String, Object>> getDeptOfficeApply(@Param("page") Page page,@Param("userId") String userId,@Param("uri")String uri);
	/**
	 * 查询用户是否是车辆管理员
	 * @param userId
	 * @return
	 */
	public int getIsVechicleAdmin(String userId);
	/**
	 * 车辆管理员查看申请信息
	 * @param userId
	 * @return
	 */
	public  Page<Map<String, Object>>  getAplyVechicleAdmin(@Param("page") Page page,@Param("userId") String userId,@Param("uri")String uri);
	
	public List<String> userRelationships(String userId);
	/**
	 * 后勤主管人员审批
	 * @param userId
	 * @return
	 */
	public Page<Map<String, Object>> personInCharge(@Param("page") Page page,@Param("userId") String userId,@Param("uri")String uri);
	/**
	 * 查询综管总监审批记录和可审核的申请
	 * @param userId
	 * @return
	 */
	public Page<Map<String, Object>>  comprehensiveAdmin(@Param("page") Page page,@Param("uri")String uri);
	/**
	 * 查询用户是否是综管
	 * @param userId
	 * @return
	 */
	public int isComprehensiveAdmin(String userId);
	/**
	 * 管理员审核
	 * @param userId
	 * @return
	 */
	public int deptAdminExamine(OfficialVehicleRecords officialVehicleRecords) ;
	
	
	public String getSuperiorUser(String userId);
	/**
	 * 申请延期
	 * @param privateCarUse
	 * @return
	 */
	public int delayApply(PrivateCarUse privateCarUse);
	/**
	 * 立即换车修改公用用车申请记录表
	 * @param privateCarUse
	 * @return
	 */
	public int returnVechicleOffice(OfficialVehicleRecords officialVehicleRecords);
	/**
	 * 修改车辆状态
	 * @return
	 */
	public int updateVechilcleStatus(@Param("id")Long id ,@Param("status") String status);
	/**
	 *   我的审批  公务用车
	 * @param page
	 * @param userId
	 * @return
	 */
	public Page<Map<String, Object>> getAllApproval(@Param("page") Page page,@Param("userId") String userId,@Param("uri") String uri);
	
	/**
	 * 公务用车个人申请查询
	 * @param userId
	 * @return
	 */
	public Page<Map<String, Object>>  getAllApplyInfoByUserId(@Param("page") Page page,@Param("userId") String userId);
	//判断用户是否上传身份证和驾驶证
	public int queryDriverAndIDCard(String userId);
	
	public int updateUserImgInfoByUserId(UserDto userDto);
	
	
	public Map<String, Object> getOffInfoById(@Param("id") String id,@Param("uri") String uri);
	
	public int cancelApplyOffical(Long id);
}
