package cn.stylefeng.guns.modular.system.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import cn.stylefeng.guns.core.interceptor.RestApiInteceptor;
import cn.stylefeng.guns.core.util.JwtTokenUtil;
import cn.stylefeng.guns.modular.system.entity.OfficialVehicleRecords;
import cn.stylefeng.guns.modular.system.entity.PrivateCarUse;
import cn.stylefeng.guns.modular.system.model.UserDto;
import cn.stylefeng.guns.modular.system.service.OfficialVehicleRecordsService;
import cn.stylefeng.guns.modular.system.service.PrivateCarUseService;
import cn.stylefeng.guns.modular.system.service.YmlUtils;
import cn.stylefeng.roses.core.base.controller.BaseController;
import cn.stylefeng.roses.core.reqres.response.ResponseData;
import io.swagger.annotations.Api;

@RestController
@RequestMapping("/officeVechicle")
@Api("公务用车")
@CrossOrigin
@Validated
public class OfficialVehicleRecordsController extends BaseController{
	
	
	@Autowired
	private OfficialVehicleRecordsService officialVehicleRecordsService;
	
	@Autowired
	private PrivateCarUseService privateCarUseService;
	@Autowired
	private YmlUtils ymlUtils;
	/**
	 * 公务用车申请
	 * @param officialVehicleRecords
	 * @return
	 */
	@PostMapping("/apply")
	public ResponseData vechicleAdd(@RequestBody OfficialVehicleRecords officialVehicleRecords,HttpServletRequest request) {
		String userId = RestApiInteceptor.getUserId(request);
		officialVehicleRecords.setApplyUser(userId);
	    officialVehicleRecordsService.saveApplyInfo(officialVehicleRecords);
	    return SUCCESS_TIP;
	}
	/**
	 * 查询自己可以审批的公务用车申请
	 * @return
	 */
	@GetMapping(value = "/director")
	public ResponseData queryNotApproval(HttpServletRequest request,Page page) {
		String uri = ymlUtils.getUri();
		String userId = RestApiInteceptor.getUserId(request);
		//根据userId查看用户角色
		UserDto listRole = officialVehicleRecordsService.queryUserRole(userId);
		if(listRole != null) {
			if ( listRole.getRoleId()==null && "0".equals(listRole.getIsManageVechicle())) {
				return ResponseData.success(null);
			}
		}else {
			return ResponseData.success(null);
		}
		//查询此用户是否是部门管理员
		int i = officialVehicleRecordsService.getUserDeptAdmin(userId);
		if(i==1) {
			//查询部门中的所有未审核的用车申请
			Page<Map<String, Object>> list = officialVehicleRecordsService.getDeptOfficeApply(userId,page,uri);
			return ResponseData.success(list);
		}
		//查询用户是否是车辆管理员
		int j = officialVehicleRecordsService.getIsVechicleAdmin(userId);
		if(j==1) {
			//查询部门管理员已审核过的所有申请
			Page<Map<String, Object>> list  = officialVehicleRecordsService.getAplyVechicleAdmin(userId, page,uri);
			return ResponseData.success(list);
		}
		//查询车辆管理员的上级用户
		List<String> listUser = officialVehicleRecordsService.userRelationships(userId);
		if(listUser != null && listUser.size()!=0) {
			//后勤审核的申请查询
			Page<Map<String, Object>> list  = officialVehicleRecordsService.personInCharge(page,userId,uri);
			return ResponseData.success(list);
		}
		//综管可审批的申请查询
		int k = officialVehicleRecordsService.isComprehensiveAdmin(userId);
		if(k==1) {
			Page<Map<String, Object>> list  = officialVehicleRecordsService.comprehensiveAdmin(page,uri);
			return ResponseData.success(list);
		}
		return ResponseData.success(null);
	}
	/**
	 * 公务用车审核
	 */
	@PostMapping("/examine")
	public ResponseData userVechicleToExamine(@RequestBody OfficialVehicleRecords officialVehicleRecords ,HttpServletRequest request) {
		String userId = RestApiInteceptor.getUserId(request);
		//查询用户是否是部门管理员
		int i = officialVehicleRecordsService.getUserDeptAdmin(userId);
		if(i==1) {
			officialVehicleRecords.setStatus(officialVehicleRecords.getAuditStatus());
			officialVehicleRecords.setDeptReviewedUser(userId);
			officialVehicleRecordsService.adminAudit(officialVehicleRecords);
			if("2".equals(officialVehicleRecords.getAuditStatus())) {
				officialVehicleRecordsService.updateVechilcleStatusByOffApplyId(officialVehicleRecords.getId(), "1");
			}
			return SUCCESS_TIP;
		}
		//查询用户是否是车辆管理员
		int j = officialVehicleRecordsService.getIsVechicleAdmin(userId);
		if(j==1) {
			//查询此用户关联关系表中的上级
			if (userId.equals(officialVehicleRecordsService.getSuperiorUser(userId))) {
				officialVehicleRecords.setVechicleAdminStatus(officialVehicleRecords.getAuditStatus());
				officialVehicleRecords.setVechicleAdminUser(userId);
				officialVehicleRecords.setLogisticsStatus(officialVehicleRecords.getAuditStatus());
				officialVehicleRecords.setLogisticsUser(userId);
				officialVehicleRecordsService.adminAudit(officialVehicleRecords);
				if("2".equals(officialVehicleRecords.getAuditStatus())) {
					officialVehicleRecordsService.updateVechilcleStatusByOffApplyId(officialVehicleRecords.getId(), "1");
				}
				return SUCCESS_TIP;
			}else {
				officialVehicleRecords.setVechicleAdminStatus(officialVehicleRecords.getAuditStatus());
				officialVehicleRecords.setVechicleAdminUser(userId);
				officialVehicleRecordsService.adminAudit(officialVehicleRecords);
				if("2".equals(officialVehicleRecords.getAuditStatus())) {
					officialVehicleRecordsService.updateVechilcleStatusByOffApplyId(officialVehicleRecords.getId(), "1");
				}
				return SUCCESS_TIP;
			}
		}
		//查询此用户是否属于某个用户的上级
		List<String> superiorUser = officialVehicleRecordsService.userRelationships(userId);
		if (superiorUser != null && superiorUser.size() != 0) {
			if (!userId.equals(superiorUser.get(0))) {
				officialVehicleRecords.setLogisticsStatus(officialVehicleRecords.getAuditStatus());
				officialVehicleRecords.setLogisticsUser(userId);
				officialVehicleRecordsService.adminAudit(officialVehicleRecords);
				if("2".equals(officialVehicleRecords.getAuditStatus())) {
					officialVehicleRecordsService.updateVechilcleStatusByOffApplyId(officialVehicleRecords.getId(), "1");
				}
				return SUCCESS_TIP;
			}
		}
		//查询用户是否是综管
		int k = officialVehicleRecordsService.isComprehensiveAdmin(userId);
		if (k==1) {
			officialVehicleRecords.setSynthesizeStatus(officialVehicleRecords.getAuditStatus());
			officialVehicleRecords.setSynthesizeUser(userId);
			officialVehicleRecordsService.adminAudit(officialVehicleRecords);
			if("2".equals(officialVehicleRecords.getAuditStatus())) {
				officialVehicleRecordsService.updateVechilcleStatusByOffApplyId(officialVehicleRecords.getId(), "1");
			}
			return SUCCESS_TIP;
			
		}
		return null;
	}
	
	/**
	 * 查询用户所有的申请记录 公务用车
	 * @param request
	 * @return
	 */
	@GetMapping("/allApplyInfo")
	public ResponseData getAllApplyInfoByUserId(HttpServletRequest request,Page page) {
		String userId = RestApiInteceptor.getUserId(request);
		return ResponseData.success(officialVehicleRecordsService.getAllApplyInfoByUserId(page, userId));
	}
	/**
	 * 查看用户已审批通过的所有记录 公务用车
	 * @param request
	 * @param page
	 * @return
	 */
	@GetMapping("/applyInfo")
	public ResponseData getAllApproval(HttpServletRequest request,Page page) {
		String userId = RestApiInteceptor.getUserId(request);
		return ResponseData.success(officialVehicleRecordsService.getAllApproval(page, userId));
	}
	
	/**
	 * 立即还车
	 * @param privateCarUse
	 * @return
	 */
	@PostMapping("/return")
	public ResponseData returnVechicle(OfficialVehicleRecords officialVehicleRecords,
		@RequestParam("file") MultipartFile file) {
		//type:1 里程图片   2：车辆外观图片
//		if ("2".equals(officialVehicleRecords.getType())) {
//			privateCarUseService.returnVechiclePrivate(privateCarUse);
//			return SUCCESS_TIP;
//		}else if("1".equals(officialVehicleRecords.getType())){
//			officialVehicleRecordsService.returnVechicleOffice(privateCarUse, appearanceFile, appearanceFile);
			return ResponseData.success(officialVehicleRecordsService.returnVechicleOffice(officialVehicleRecords, file , officialVehicleRecords.getType()));
//		}
//		return null;
	}
	/**
	 * 查看驾驶证和身份证是否上传
	 * @param request
	 * @return
	 */
	@GetMapping("/isUpload")
	public ResponseData queryDriverAndIDCard(HttpServletRequest request) {
		String userId = RestApiInteceptor.getUserId(request);
		int i = officialVehicleRecordsService.queryDriverAndIDCard(userId);
		if(i==0) {
			return ResponseData.success(0, "请先上传身份证图片和驾驶证图片", null);
		}else {
			return ResponseData.success(1, "", null);
		}
	}
	/**
	 * 根据id查询申请信息
	 * @param request
	 * @return
	 */
	@GetMapping("/getInfoById")
	public ResponseData queryDriverAndIDCard(String id) {
		String uri = ymlUtils.getUri();
		return ResponseData.success(officialVehicleRecordsService.getOffInfoById(id, uri));
	}
	
	/**
	 * 取消用车申请
	 * @param id
	 * @return
	 */
	@GetMapping("/cancelApply")
	public ResponseData cancelApply(Long id,Integer type) {
		//type 1：公务用车   2：私车公用
		if(type==1) {
			officialVehicleRecordsService.cancelApplyOffical(id);
		}else if(type==2){
			privateCarUseService.cancelApplyPrivate(id);
		}
		return SUCCESS_TIP;
	}
	
	
}
