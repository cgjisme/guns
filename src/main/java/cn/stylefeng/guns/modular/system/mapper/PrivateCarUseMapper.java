package cn.stylefeng.guns.modular.system.mapper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import cn.stylefeng.guns.modular.system.entity.OfficialVehicleRecords;
import cn.stylefeng.guns.modular.system.entity.PrivateCarUse;

public interface PrivateCarUseMapper extends BaseMapper<PrivateCarUse> {
	
	public int savePrivateCarUseInfo(PrivateCarUse privateCarUse);
	

	/**
	 * 私车公用 待校对申请信息查询
	 * @param userId
	 * @return
	 */
	public Page<Map<String, Object>> toBeProofread(@Param("page") Page page,@Param("region") Integer region,@Param("uri")String uri );
	
	/**
	 * 查询用户所属区域
	 * @param userId
	 * @return
	 */
	public Integer getUserRegion(String userId);
	/**
	 * 私车公用 部门总监待审核申请查询
	 * @param userId
	 * @return
	 */
	public Page<Map<String, Object>> findPriCarUserInfoByDeptAdmin(@Param("page") Page page,@Param("userId") String userId,@Param("uri")String uri );
	
	public List<Integer> whetherNotify(String userId);
	
	public Page<Map<String, Object>> findNotifyAdminAuditInfo(@Param("page") Page page,@Param("regionList") List<Integer> regionList,@Param("uri")String uri );

	public int updateStatus( PrivateCarUse privateCarUse);
	
	public Page<Map<String, Object>>  getAllApplyInfoByUserId(@Param("page") Page page,@Param("userId")String userId);
	
	
	public int delayApply(PrivateCarUse privateCarUse) ;
	/**
	 * 私车公用 还车
	 * @return
	 */
	public int returnVechiclePrivate(PrivateCarUse privateCarUse);
	
	public Page<Map<String, Object>> getAllApproval(@Param("page") Page page,@Param("userId")String userId,@Param("uri") String uri);

	public Map<String, Object> getPrivInfoById(@Param("id")String id,@Param("uri") String uri);
	
	public int cancelApplyPrivate(Long id);
}
