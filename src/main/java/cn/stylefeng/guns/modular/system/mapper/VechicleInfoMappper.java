package cn.stylefeng.guns.modular.system.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import cn.stylefeng.guns.modular.system.entity.Role;
import cn.stylefeng.guns.modular.system.entity.User;
import cn.stylefeng.guns.modular.system.entity.VechicleInfo;
import cn.stylefeng.guns.modular.system.model.VechicleInfoDto;
/**
 * 
 * @author zhaozhenshe
 *
 */
public interface VechicleInfoMappper extends BaseMapper<VechicleInfo>{
	/**
	 * 查询车辆信息
	 * @return
	 */
	Page<Map<String, Object>> selectVechicleInfos(@Param("page") Page page,@Param("roleName")String roleName);
	
	void updateVechicleStatus(@Param("id")Long id ,@Param("status") String status);
	
	/**
     * 查询所有的车辆管理员
     * @return
     */
    public List<User> queryVechicleAdmin();
    /**
     * 车辆信息修改
     * @param vechicleInfoDto
     */
    public void vechicleUpdate(VechicleInfoDto vechicleInfoDto);
    /**
     * 添加车辆
     * @param vechicleInfo
     */
    public void addVechicle(VechicleInfoDto vechicleInfo) ;
    
    public List<String> queryVechicleType();
    
    public List<VechicleInfoDto> queryPlateNumber(String type);
    
}
