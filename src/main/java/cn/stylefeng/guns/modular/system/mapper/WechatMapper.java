package cn.stylefeng.guns.modular.system.mapper;

import java.util.List;

import cn.stylefeng.guns.modular.system.entity.Dept;
import cn.stylefeng.guns.modular.system.entity.User;
import cn.stylefeng.guns.modular.system.entity.WechatUser;
import cn.stylefeng.guns.modular.system.model.DeptDto;

public interface WechatMapper {
	
	public int addWechatUser(WechatUser user);
	
	public WechatUser getUserInfoByUserId( String userId) ;
	
	public List<DeptDto> getAllDept();
}
