package cn.stylefeng.guns.modular.system.service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import cn.stylefeng.guns.core.common.exception.BizExceptionEnum;
import cn.stylefeng.guns.core.common.page.LayuiPageFactory;
import cn.stylefeng.guns.core.shiro.ShiroKit;
import cn.stylefeng.guns.modular.system.entity.Dept;
import cn.stylefeng.guns.modular.system.entity.User;
import cn.stylefeng.guns.modular.system.entity.WechatUser;
import cn.stylefeng.guns.modular.system.mapper.WechatMapper;
import cn.stylefeng.guns.modular.system.model.DeptDto;
import cn.stylefeng.guns.modular.system.warpper.DeptWrapper;
import cn.stylefeng.roses.core.reqres.response.ResponseData;
import cn.stylefeng.roses.kernel.model.exception.ServiceException;

@Component
@Transactional
public class WechatUserService {
	
	@Autowired
	private WechatMapper wechatMapper;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private YmlUtils ymlUtils;
	
	public Long addWechatUser(WechatUser user) {
		// 完善账号信息
//        String salt = ShiroKit.getRandomSalt(5);
//        String password = ShiroKit.md5(user.getPassword(), salt);
//        user.setPassword(password);
//        user.setSalt(salt);
		// 判断账号是否重复
        User theUser = userService.getByAccount(user.getAccount());
        if (theUser != null) {
            throw new ServiceException(BizExceptionEnum.USER_ALREADY_REG);
        }
        // 完善账号信息
        String salt = ShiroKit.getRandomSalt(5);
        String password = ShiroKit.md5(user.getPassword(), salt);
        user.setPassword(password);
        user.setSalt(salt);
        wechatMapper.addWechatUser(user);
		return user.getUserId();
	}
	
	public int addWechatUser2(WechatUser user,
			 MultipartFile idCardFrontFile,
			 MultipartFile idCardBackFile,
			MultipartFile driverLicenseFile
			) throws IOException, Exception {
		
		//身份证正面照片上传
		String fileName = System.currentTimeMillis() + idCardFrontFile.getOriginalFilename();
		String path = ymlUtils.getIdentityCard();
		File  destFile = new File(path);
        //如果文件夹不存在则创建    
		if (!destFile.exists() && !destFile.isDirectory()) {
			System.out.println("//不存在");
			destFile.mkdir();
		} else {
			System.out.println("//目录存在");
		}
		FileUtil.uploadFile(idCardFrontFile.getBytes(), path, fileName);
		user.setIdCardFrontUrl(path+fileName);
		
		//身份证背面照片上传
		String fileName2 = System.currentTimeMillis() + idCardBackFile.getOriginalFilename();
		String path2 = ymlUtils.getIdentityCard();
		File  destFile2 = new File(path2);
        //如果文件夹不存在则创建    
		if (!destFile2.exists() && !destFile2.isDirectory()) {
			System.out.println("//不存在");
			destFile2.mkdir();
		} else {
			System.out.println("//目录存在");
		}
		FileUtil.uploadFile(idCardBackFile.getBytes(), path2, fileName2);
		user.setIdCardBackUrl(path2+fileName2);
		
		//驾驶证照片上传
		String fileName3 = System.currentTimeMillis() + driverLicenseFile.getOriginalFilename();
		String path3 = ymlUtils.getIdentityCard();
		File  destFile3 = new File(path3);
        //如果文件夹不存在则创建    
		if (!destFile3.exists() && !destFile3.isDirectory()) {
			System.out.println("//不存在");
			destFile3.mkdir();
		} else {
			System.out.println("//目录存在");
		}
		FileUtil.uploadFile(driverLicenseFile.getBytes(), path3, fileName3);
		user.setDriverLicenseUrl(path3+fileName3);
		
		//头像
//		String fileName4 = System.currentTimeMillis() + avatar.getOriginalFilename();
//		String path4 = ymlUtils.getIdentityCard();
//		File  destFile4 = new File(path4);
//        //如果文件夹不存在则创建    
//		if (!destFile4.exists() && !destFile4.isDirectory()) {
//			System.out.println("//不存在");
//			destFile4.mkdir();
//		} else {
//			System.out.println("//目录存在");
//		}
//		FileUtil.uploadFile(avatar.getBytes(), path4, fileName4);
//		user.setAvatar(path4+fileName4);
		
		// 完善账号信息
        String salt = ShiroKit.getRandomSalt(5);
        String password = ShiroKit.md5(user.getPassword(), salt);
        user.setPassword(password);
        user.setSalt(salt);
		return wechatMapper.addWechatUser(user);
	}
	public WechatUser getUserInfoByUserId( String userId) {
		return wechatMapper.getUserInfoByUserId(userId);
	}
	
	public List<DeptDto> getAllDept() {
        return wechatMapper.getAllDept();
    }
}
