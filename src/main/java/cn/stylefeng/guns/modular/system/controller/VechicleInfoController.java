package cn.stylefeng.guns.modular.system.controller;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import cn.hutool.core.bean.BeanUtil;
import cn.stylefeng.guns.core.common.annotion.BussinessLog;
import cn.stylefeng.guns.core.common.annotion.Permission;
import cn.stylefeng.guns.core.common.constant.Const;
import cn.stylefeng.guns.core.common.constant.dictmap.DeleteDict;
import cn.stylefeng.guns.core.common.constant.dictmap.RoleDict;
import cn.stylefeng.guns.core.common.constant.factory.ConstantFactory;
import cn.stylefeng.guns.core.common.exception.BizExceptionEnum;
import cn.stylefeng.guns.core.common.page.LayuiPageFactory;
import cn.stylefeng.guns.core.log.LogObjectHolder;
import cn.stylefeng.guns.core.shiro.ShiroKit;
import cn.stylefeng.guns.core.shiro.ShiroUser;
import cn.stylefeng.guns.modular.system.entity.Role;
import cn.stylefeng.guns.modular.system.entity.User;
import cn.stylefeng.guns.modular.system.entity.VechicleInfo;
import cn.stylefeng.guns.modular.system.model.RoleDto;
import cn.stylefeng.guns.modular.system.model.VechicleInfoDto;
import cn.stylefeng.guns.modular.system.service.UserService;
import cn.stylefeng.guns.modular.system.service.VechicleInfoServrice;
import cn.stylefeng.roses.core.base.controller.BaseController;
import cn.stylefeng.roses.core.reqres.response.ResponseData;
import cn.stylefeng.roses.core.util.ToolUtil;
import cn.stylefeng.roses.kernel.model.exception.ServiceException;

/**
 * 车辆控制器
 * @author zhaozhenshe
 *
 */
@Controller
@RequestMapping("/vechicle")
public class VechicleInfoController extends BaseController{
	

	@Autowired
	private VechicleInfoServrice vechicleInfoServrice;
	
	@Autowired
	private UserService userService;

    private String PREFIX = "/modular/system/vechicle/";
    /**
     * 跳转车辆管理主页面
     * @return
     */
    @RequestMapping("/page")
    public String index() {
        return PREFIX + "vechicle.html";
    }
    /**
     * 添加页面
     * @return
     */
    @RequestMapping("/add")
    public String vechicleAdd() {
        return PREFIX + "vechicle_add.html";
    }
    
    @GetMapping("/list")
    @ResponseBody
    public Object vechicleQuery(@RequestParam(value = "roleName", required = false) String roleName) {
    	  Page<Map<String, Object>> roles = vechicleInfoServrice.selectVechPage(roleName);
//          Page<Map<String, Object>> wrap = new RoleWrapper(roles).wrap();
          return LayuiPageFactory.createPageInfo(roles);
    }
    /**
     * 删除车辆
     */
    @RequestMapping(value = "/remove")
    @Permission(Const.ADMIN_NAME)
    @ResponseBody
    public ResponseData remove(@RequestParam Long id) {
        //缓存被删除的部门名称
//        LogObjectHolder.me().set(ConstantFactory.me().getP());

    	vechicleInfoServrice.remove(id);
        return SUCCESS_TIP;
    }
    
    @Permission
    @RequestMapping(value = "/edit")
    public String roleEdit(@RequestParam Long id) {
        if (ToolUtil.isEmpty(id)) {
            throw new ServiceException(BizExceptionEnum.REQUEST_NULL);
        }
        VechicleInfo role = this.vechicleInfoServrice.getById(id);
        LogObjectHolder.me().set(role);
        return PREFIX + "vechicle_edit.html";
    }
    
    /**
     * 查看角色
     *
     * @author fengshuonan
     * @Date 2018/12/23 6:31 PM
     */
    @RequestMapping(value = "/view/{roleId}")
    @ResponseBody
    public ResponseData view(@PathVariable Long roleId) {
        if (ToolUtil.isEmpty(roleId)) {
            throw new ServiceException(BizExceptionEnum.REQUEST_NULL);
        }
        VechicleInfo role  = this.vechicleInfoServrice.getById(roleId);
        User user = userService.getById(role.getAdmin());
        Map<String,String> adminMap = new LinkedHashMap<>();
        if(user!=null) {
        	adminMap.put(role.getAdmin(), user.getName());
        }
        //查询所有的车辆管理员
        List<User> list = vechicleInfoServrice.queryVechicleAdmin();
        for (User user2 : list) {
        	adminMap.put(user2.getUserId().toString(), user2.getName());
		}
        VechicleInfoDto vechicleInfoDto = new VechicleInfoDto();
        vechicleInfoDto.setAdminMap(adminMap);
        vechicleInfoDto.setId(role.getId());
        vechicleInfoDto.setPlateNumber(role.getPlateNumber());
        vechicleInfoDto.setVehicleType(role.getVehicleType());
        vechicleInfoDto.setPurpose(role.getPurpose());
        vechicleInfoDto.setRegionForUse(role.getRegionForUse());
        vechicleInfoDto.setAdmin(role.getAdmin());
        Map<String, Object> roleMap = BeanUtil.beanToMap(vechicleInfoDto);

//        roleMap.put("plateNumber", role.getPlateNumber());
//        roleMap.put("vehicleType", role.getVehicleType());
//        roleMap.put("purpose", role.getPurpose());
//        roleMap.put("regionForUse", role.getRegionForUse());
//        roleMap.put("id", role.getId());
//        if("4".equals(role.getStatus()) || "0".equals(role.getStatus())) {
//        	roleMap.put("isSendCar", "4");
//        }else {
//        	roleMap.put("isSendCar", "1");
//        }
        return ResponseData.success(roleMap);
    }
    @PostMapping("/update")
    @Permission(Const.ADMIN_NAME)
    @ResponseBody
    public ResponseData vechicleUpdate(VechicleInfoDto vechicleInfoDto) {
    	if("".equals(vechicleInfoDto.getAdmin())) {
    		vechicleInfoDto.setAdmin(null);
    	}
        vechicleInfoServrice.vechicleUpdate(vechicleInfoDto);
        return SUCCESS_TIP;
    }
    /**
     * 查询所有的车辆管理员
     * @param roleId
     * @return
     */
    @RequestMapping(value = "/allAdminVechicle")
    @ResponseBody
    public ResponseData queryAllAdminVechicle() {
    	//查询所有的车辆管理员
        List<User> list = vechicleInfoServrice.queryVechicleAdmin();
        return ResponseData.success(list);
    }
    /**
     * 添加车辆信息
     * @param role
     * @return
     */
    @RequestMapping(value = "/addVechicle")
    @ResponseBody
    public ResponseData addVechicle(VechicleInfoDto vechicleInfo) {
        this.vechicleInfoServrice.addVechicle(vechicleInfo);
        return SUCCESS_TIP;
    }
    /**
     * 车辆类型查询
     * @param vechicleInfo
     * @return
     */
    @RequestMapping(value = "/vechType")
    @ResponseBody
    public ResponseData queryVechicleType() {
        return ResponseData.success(this.vechicleInfoServrice.queryVechicleType());
    }
    /**
     * 根据车辆类型查询车牌号码
     * @return
     */
    @RequestMapping(value = "/plateNumber")
    @ResponseBody
    public ResponseData queryPlateNumberByType(String type) {
        return ResponseData.success(this.vechicleInfoServrice.queryPlateNumber(type));
    }
}
