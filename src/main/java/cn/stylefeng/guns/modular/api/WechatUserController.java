package cn.stylefeng.guns.modular.api;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import cn.stylefeng.guns.config.web.GetTokenInfo;
import cn.stylefeng.guns.core.common.annotion.Permission;
import cn.stylefeng.guns.core.common.page.LayuiPageFactory;
import cn.stylefeng.guns.modular.system.entity.WechatUser;
import cn.stylefeng.guns.modular.system.service.OfficialVehicleRecordsService;
import cn.stylefeng.guns.modular.system.service.WechatUserService;
import cn.stylefeng.guns.modular.system.warpper.DeptWrapper;
import cn.stylefeng.roses.core.reqres.response.ResponseData;
import io.swagger.annotations.Api;

@RestController
@RequestMapping("/wechat")
@Api("小程序用户")
@CrossOrigin
@Validated
public class WechatUserController {
	
	@Autowired
	private WechatUserService wechatUserService;
	@Autowired
	private OfficialVehicleRecordsService officialVehicleRecordsService;
	@PostMapping("/add")
    public ResponseData addWechatUser(@RequestBody WechatUser user,HttpServletRequest request) {
//		System.out.println(GetTokenInfo.getUserId(request));
        return ResponseData.success(wechatUserService.addWechatUser(user));
    }
	
	@PostMapping("/add2")
    public String addWechatUser2( WechatUser user,
    		@RequestParam("idCardFrontUrl") MultipartFile idCardFrontFile,
			@RequestParam("idCardBackFile") MultipartFile idCardBackFile,
			@RequestParam("driverLicenseFile") MultipartFile driverLicenseFile
    		) throws IOException, Exception {
        return String.valueOf(wechatUserService.addWechatUser2
        		(user, idCardFrontFile, idCardBackFile, driverLicenseFile));
    }
	
	@GetMapping("/userInfo")
    public ResponseData getUserInfoByUserId( HttpServletRequest request) {
		String userId = GetTokenInfo.getUserId(request);
		return ResponseData.success(wechatUserService.getUserInfoByUserId(userId));
	}
	
	 /**
     * 获取所有部门列表
     *
     * @author fengshuonan
     * @Date 2018/12/23 4:57 PM
     */
    @RequestMapping(value = "/listDept")
    @ResponseBody
    public Object list() {
       return ResponseData.success(wechatUserService.getAllDept());
    }
    
    @PostMapping("/upload")
	public ResponseData uploadImages(@RequestParam("userId")String userId,
			@RequestParam("file") MultipartFile file, @RequestParam("type")String type) {
//		 = RestApiInteceptor.getUserId(request);
		//1: 驾驶证    2：身份证正面 3：身份证反面 4：头像
		try {
			return ResponseData.success(officialVehicleRecordsService.uploadImages(file, type, userId));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseData.success(0, "业务异常", null);
		}
	}
}
