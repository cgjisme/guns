/**
 * 角色详情对话框
 */
var RoleInfoDlg = {
    data: {
        pid: "",
        pName: ""
    }
};

layui.use(['layer', 'form', 'admin', 'ax'], function () {
    var $ = layui.jquery;
    var $ax = layui.ax;
    var form = layui.form;
    var admin = layui.admin;
    var layer = layui.layer;

    // 让当前iframe弹层高度适应
    admin.iframeAuto();

    //初始化角色的详情数据
    var ajax = new $ax(Feng.ctxPath + "/vechicle/view/" + Feng.getUrlParam("id"));
    var result = ajax.start();
    
    var data = result.data.adminMap;
    var str = "<option value=''>请选择</option>";
	if(result.data.admin!="" && result.data.admin!=null ){
		for(var key in data){
			if(result.data.admin==key){
				str+="<option value='"+key+"' selected>"+data[key]+"</option>";
			}else{
				str+="<option value='"+key+"'>"+data[key]+"</option>";
			}
	    }
	}else{
		for(var key in data){
	    	str+="<option value='"+key+"'>"+data[key]+"</option>"
	    }
	}	      
    $("#selected_admin").html(str)
    form.val('roleForm',result.data);

    // 表单提交事件
    form.on('submit(btnSubmit)', function (data) {
        var ajax = new $ax(Feng.ctxPath + "/vechicle/update", function (data) {
            Feng.success("修改成功!");

            //传给上个页面，刷新table用
            admin.putTempData('formOk', true);

            //关掉对话框
            admin.closeThisDialog();
        }, function (data) {
            Feng.error("修改失败!" + data.responseJSON.message + "!");
        });
        ajax.set(data.field);
        ajax.start();
    });
});